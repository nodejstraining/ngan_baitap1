var express= require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var path = require('path');
var session = require('express-session');
var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
var passport = require('passport');


var configDB = require('./models/database');
mongoose.connect(configDB.url);

var port = process.env.PORT||5000;

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(session({
  secret:'secret',
  saveUninitialized:true,
  resave:true
}))

app.use(passport.initialize());
app.use(passport.session());

app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));
app.use(express.static(__dirname + '/public'));

var login = require('./controllers/login');
var signup = require('./controllers/signup');
var upload = require('./controllers/upload');

app.use('/user',login);
app.use('/signup',signup);
app.use('/upload',upload);

app.listen(port);
console.log('Server running port ' +port);
